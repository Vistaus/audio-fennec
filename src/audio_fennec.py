'''
 Copyright (C) 2019  Brian Douglass

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 ubuntu-calculator-app is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
import os
import json
import pyotherside

path = os.path.join(os.path.dirname(__file__), '../.venv/lib/python3.6/site-packages/')
sys.path.insert(0, path)

from podfox import PodFox

def init():
    podfox = PodFox()
    config_file = os.path.join(os.path.dirname(__file__), 'config.json')
    podfox.parse_config(config_file)

    return podfox


def setup_podcast_dir():
    path = init().CONFIGURATION['podcast-directory']
    if not os.path.exists(path):
        os.makedirs(path)


def feeds():
    setup_podcast_dir()
    feeds = init().available_feeds()

    for feed in feeds:
        feed['dtitle'] = feed['title']
        feed['unlistened'] = len([ep for ep in feed['episodes'] if not ep['listened']])
        feed['hasUnlistened'] = feed['unlistened'] > 0
        feed['episodes'] = feed['episodes'][:10]

    feeds = sorted(feeds, key=lambda k: (not k['hasUnlistened'], k['title']))

    return json.dumps(feeds)


def add_feed(url):
    init().import_feed(url)


def update_feeds():
    podfox = init()
    for feed in podfox.available_feeds():
        pyotherside.send('updating', feed['shortname'])
        print('Updating feed', feed['shortname'])
        podfox.update_feed(feed)

    pyotherside.send('updating', '')
    return feeds()


def find_feed(shortname):
    podfox = init()
    feed = podfox.find_feed(shortname)
    feed['episodes'] = feed['episodes'][:10]

    return json.dumps(feed)


def update_feed(shortname):
    podfox = init()
    feed = podfox.find_feed(shortname)

    pyotherside.send('updating', feed['shortname'])
    podfox.update_feed(feed)
    pyotherside.send('updating', '')

    return find_feed(shortname)


def download_feed(shortname):
    podfox = init()
    feed = podfox.find_feed(shortname)

    # Not using download_multiple here so the interface can be updated for each download
    #podfox.download_multiple(feed)

    maxnum = podfox.CONFIGURATION['maxnum']

    for episode in feed['episodes']:
        if maxnum == 0:
            break

        if not episode['downloaded'] and not episode['listened']:
            pyotherside.send('downloading', episode['url'], find_feed(shortname))
            filename = podfox.download_single(feed['shortname'], episode['url'])
            episode['downloaded'] = True
            episode['filename'] = filename
            maxnum -= 1

            podfox.overwrite_config(feed)

    pyotherside.send('downloading', '', find_feed(shortname))
    return find_feed(shortname)


def delete_feed(shortname):
    podfox = init()
    feed = podfox.find_feed(shortname)
    init().delete_feed(feed)

    return feeds()


def mark_episode_listened(shortname, episode_title):
    podfox = init()
    feed = podfox.find_feed(shortname)
    podfox.mark_episode_listened(feed, episode_title)

    return find_feed(shortname)

