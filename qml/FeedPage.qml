import QtQuick 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

Page {
    id: feedPage

    property string shortname
    property var feed: { episodes: [] }
    property bool refreshing: false
    property string downloadingUrl

    Component.onCompleted: findFeed()

    Connections {
        target: python
        onDownloading: {
            downloadingUrl = url;
            feedPage.feed = JSON.parse(feed);
        }
    }

    function findFeed() {
        refreshing = true;

        python.call('audio_fennec.find_feed', [shortname], function(f) {
            feed = JSON.parse(f);
            refreshing = false;
        });
    }

    function updateFeed() {
        refreshing = true;

        python.call('audio_fennec.update_feed', [feed.shortname], function(f) {
            feed = JSON.parse(f);
            refreshing = false;
        });
    }

    function downloadFeed() {
        python.call('audio_fennec.download_feed', [feed.shortname], function(f) {
            feed = JSON.parse(f);
        });
    }

    function markListened(episodeTitle) {
        python.call('audio_fennec.mark_episode_listened', [feed.shortname, episodeTitle], function(f) {
            feed = JSON.parse(f);
        });
    }

    header: PageHeader {
        id: header
        title: feed.title ? feed.title : ''

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                },

                Action {
                    text: i18n.tr('Download')
                    iconName: 'save'

                    onTriggered: downloadFeed()
                    // TODO hide if nothing to download
                },

                Action {
                    text: i18n.tr('Update')
                    iconName: 'view-refresh'

                    onTriggered: updateFeed()
                }
            ]
        }
    }

    Label {
        anchors.centerIn: parent
        width: parent.width
        visible: !refreshing && feed.episodes.length === 0

        text: i18n.tr('No episodes yet')

        wrapMode: Label.WordWrap
        horizontalAlignment: Label.AlignHCenter
    }

    ActivityIndicator {
        anchors.centerIn: parent
        visible: running
        running: refreshing
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true
        visible: !refreshing

        ListView {
            anchors.fill: parent
            model: feed.episodes

            delegate: ListItem {
                height: layout.height

                trailingActions: ListItemActions {
                    // TODO option to download & mark as unlistened

                    actions: Action {
                        iconName: 'tick'
                        text: i18n.tr('Mark as Listened')

                        onTriggered: markListened(modelData.title)
                        // TODO hide if listened
                    }
                }

                ListItemLayout {
                    id: layout
                    title.text: modelData.title
                    subtitle.text: {
                        var subtitle = i18n.tr('Not Downloaded');
                        if (modelData.downloaded) {
                            subtitle = i18n.tr('Downloaded');
                        }

                        if (modelData.listened) {
                            subtitle = i18n.tr('Listened');
                        }

                        return subtitle;
                    }

                    ActivityIndicator {
                        SlotsLayout.position: SlotsLayout.Trailing
                        running: downloadingUrl == modelData.url
                    }
                }
            }
        }
    }
}
